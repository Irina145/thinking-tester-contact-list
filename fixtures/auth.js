import { faker } from "@faker-js/faker";

/**
 * Метод возвращает ожидаемый текст ошибки
 * @param {Object} testdata
 * @returns Текст ошибки
 */
export function generationErrorText(testdata) {
  switch (testdata.title) {
    case "wrongPassword":
    case "wrongLogin":
      return "Incorrect username or password";
  }
}

export const invalidAuthData = [
  {
    title: "wrongPassword",
    email: `${process.env.EMAIL}`,
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "wrongLogin",
    email: faker.internet.email(),
    password: `${process.env.PASSWORD}`,
  },
];
