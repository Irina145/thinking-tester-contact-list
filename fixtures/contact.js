import { faker } from "@faker-js/faker";
import { random } from "lodash";

export function birthdateGenerator() {
  const birthdate = faker.date.birthdate();
  let date = JSON.stringify(birthdate).split("T")[0];
  return String(date).replace('"', "");
}

export const errorLongFirstName = (firstname) =>
  `Contact validation failed: firstName: Path \`firstName\` (\`${firstname}\`) is longer than the maximum allowed length (20).`;
export const errorLongLastName = (lastname) =>
  `Contact validation failed: lastName: Path \`lastName\` (\`${lastname}\`) is longer than the maximum allowed length (20).`;
export const errorLongPhone = (phone) =>
  `Contact validation failed: phone: Path \`phone\` (\`${phone}\`) is longer than the maximum allowed length (15).`;
export const errorStreetAdress1 = (streetAdress1) =>
  `Contact validation failed: street1: Path \`street1\` (\`${streetAdress1}\`) is longer than the maximum allowed length (40).`;
export const errorStreetAdress2 = (streetAdress2) =>
  `Contact validation failed: street2: Path \`street2\` (\`${streetAdress2}\`) is longer than the maximum allowed length (40).`;
export const errorLongCity = (city) =>
  `Contact validation failed: city: Path \`city\` (\`${city}\`) is longer than the maximum allowed length (40).`;
export const errorLongCountry = (country) =>
  `Contact validation failed: country: Path \`country\` (\`${country}\`) is longer than the maximum allowed length (40).`;
export const errorLongState = (state) =>
  `Contact validation failed: stateProvince: Path \`stateProvince\` (\`${state}\`) is longer than the maximum allowed length (20).`;
export const longPostalCode = (postalCode) =>
  `Contact validation failed: postalCode: Path \`postalCode\` (\`${postalCode}\`) is longer than the maximum allowed length (10).`;

/**
 * Метод возвращает ожидаемый текст ошибки
 * @param {Object} testdata
 * @returns Текст ошибки
 */
export function generationErrorText(testdata) {
  switch (testdata.title) {
    case "longFirstname":
      return errorLongFirstName(testdata.firstname);
    case "emptyFirstname":
      return "Contact validation failed: firstName: Path `firstName` is required.";
    case "emptyLastname":
      return "Contact validation failed: lastName: Path `lastName` is required.";
    case "longLastname":
      return errorLongLastName(testdata.lastname);
    case "invalidEmail@":
    case "invalidEmail@@":
    case "invalidEmailDomen":
    case "longEmail":
      return "Contact validation failed: email: Email is invalid";
    case "invalidBirtdate":
    case "invalidBirtdateOnlyYear":
    case "invalidBirtdateNotYear":
      return "Contact validation failed: birthdate: Birthdate is invalid";
    case "shortPhone":
    case "invalidPhoneAlpha":
      return "Contact validation failed: phone: Phone number is invalid";
    case "longPhone":
      return errorLongPhone(testdata.phone);
    case "longStretAdress1":
      return errorStreetAdress1(testdata.streetAdress1);
    case "longStretAdress2":
      return errorStreetAdress2(testdata.streetAdress2);
    case "longCity":
      return errorLongCity(testdata.city);
    case "longState":
      return errorLongState(testdata.stateOrProvince);
    case "longCountry":
      return errorLongCountry(testdata.country);
    case "shortPostalCode":
      return "Contact validation failed: postalCode: Postal code is invalid";
    case "longPostalCode":
      return longPostalCode(testdata.postalCode);
  }
}

export const phoneList = [
  "227-515-7603",
  "909090",
  "012345678912345",
  "+79990000000",
  "(821) 260-3638",
];

export const contactData = [
  {
    title: "onlyRequired",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },
  {
    title: "random",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: birthdateGenerator(),
    email: faker.internet.email(),
    phone: phoneList[random(0, 4)],
    streetAdress1: faker.location.streetAddress(true),
    streetAdress2: `${faker.location.street()}, ${faker.location.secondaryAddress()}`,
    city: faker.location.city(),
    stateOrProvince: faker.location.state(),
    country: faker.location.country(),
    postalCode: faker.location.zipCode(),
  },
];

export const invalidContacttData = [
  {
    title: "emptyFirstname",
    firstname: "",
    lastname: faker.person.lastName(),
    birthdate: "",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },
  {
    title: "longFirstname",
    firstname: faker.string.alpha(21),
    lastname: faker.person.lastName(),
    birthdate: "",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },
  {
    title: "emptyLastname",
    firstname: faker.person.firstName(),
    lastname: "",
    birthdate: "",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },
  {
    title: "longLastname",
    firstname: faker.person.firstName(),
    lastname: faker.string.alpha(21),
    birthdate: "",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },

  {
    title: "invalidBirtdate",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "1990-24-07",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },
  {
    title: "invalidBirtdateOnlyYear",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "1990",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },
  {
    title: "invalidBirtdateNotYear",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "04-02",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },

  {
    title: "invalidEmail@",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    email: "invalid.mail.ex",
    birthdate: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },

  {
    title: "invalidEmail@@",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    email: "invalid@mai@l.ex",
    birthdate: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },

  {
    title: "invalidEmailDomen",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    email: "invalid@mail",
    birthdate: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },

  {
    title: "shortPhone",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "",
    email: "",
    phone: faker.string.numeric({ length: { min: 1, max: 5 } }),
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },
  {
    title: "longPhone",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "",
    email: "",
    phone: faker.string.numeric({ length: { min: 16, max: 30 } }),
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },

  {
    title: "invalidPhoneAlpha",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "",
    email: "",
    phone: faker.string.alpha(10),
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },
  {
    title: "longStretAdress1",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "",
    email: "",
    phone: "",
    streetAdress1: faker.string.alpha(41),
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },
  {
    title: "longStretAdress2",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: faker.string.alpha(41),
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },
  {
    title: "longCity",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: faker.string.alpha(41),
    stateOrProvince: "",
    country: "",
    postalCode: "",
  },
  {
    title: "longState",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: faker.string.alpha(21),
    country: "",
    postalCode: "",
  },
  {
    title: "shortPostalCode",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: faker.string.numeric(2),
  },
  {
    title: "longPostalCode",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: "",
    postalCode: faker.string.numeric(11),
  },
  {
    title: "longCountry",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    birthdate: "",
    email: "",
    phone: "",
    streetAdress1: "",
    streetAdress2: "",
    city: "",
    stateOrProvince: "",
    country: faker.string.alpha(41),
    postalCode: "",
  },
];
