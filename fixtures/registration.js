import { faker } from "@faker-js/faker";

export const errorLongFirstName = (firstname) =>
  `User validation failed: firstName: Path \`firstName\` (\`${firstname}\`) is longer than the maximum allowed length (20).`;
export const errorLongLastName = (lastname) =>
  `User validation failed: lastName: Path \`lastName\` (\`${lastname}\`) is longer than the maximum allowed length (20).`;
export const errorShortPassword = (password) =>
  `User validation failed: password: Path \`password\` (\`${password}\`) is shorter than the minimum allowed length (7).`;
export const errorLongPassword = (password) =>
  `User validation failed: password: Path \`password\` (\`${password}\`) is longer than the maximum allowed length (100).`;

/**
 * Метод возвращает ожидаемый текст ошибки
 * @param {Object} testdata
 * @returns Текст ошибки
 */
export function generationErrorText(testdata) {
  switch (testdata.title) {
    case "longFirstName":
      return errorLongFirstName(testdata.firstname);
    case "emptyFirstName":
      return "User validation failed: firstName: Path `firstName` is required.";
    case "emptyLastName":
      return "User validation failed: lastName: Path `lastName` is required.";
    case "longLastName":
      return errorLongLastName(testdata.lastname);
    case "emptyEmail":
    case "invalidEmail@":
    case "invalidEmail@@":
    case "invalidEmailDomen":
    case "longEmail":
      return "User validation failed: email: Email is invalid";
    case "alreadyExistEmail":
      return "Email address is already in use";
    case "shortPassword":
      return errorShortPassword(testdata.password);
    case "longPassword":
      return errorLongPassword(testdata.password);
    case "emptyPassword":
      return "User validation failed: password: Path `password` is required.";
  }
}

export const registrationData = [
  {
    title: "random",
    email: faker.internet.email(),
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "latinFirstName",
    email: faker.internet.email(),
    firstname: "John",
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "cyrillicFirstName",
    email: faker.internet.email(),
    firstname: "Джон",
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "shortFirstName",
    email: faker.internet.email(),
    firstname: "J",
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "longFirstName",
    email: faker.internet.email(),
    firstname: "Wolfeschlsteinhausen",
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "latinLastName",
    email: faker.internet.email(),
    firstname: faker.person.firstName(),
    lastname: "Doe",
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "cyrillicLastName",
    email: faker.internet.email(),
    firstname: faker.person.firstName(),
    lastname: "Петров",
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "shortLastName",
    email: faker.internet.email(),
    firstname: faker.person.firstName(),
    lastname: "D",
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "longLastName",
    email: faker.internet.email(),
    firstname: faker.person.firstName(),
    lastname: "Wolfeschlsteinhausen",
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "shortEmail",
    email: `${faker.string.alphanumeric(1)}@${faker.string.alpha(1)}.ex`,
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "longEmail",
    email: faker.string.alphanumeric(37) + "50@example.ex",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "shortPassword",
    email: faker.internet.email(),
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 7 }),
  },
  {
    title: "longPassword",
    email: faker.internet.email(),
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 100 }),
  },
];

export const invalidRegistrationData = [
  {
    title: "longFirstName",
    email: faker.internet.email(),
    firstname: faker.string.sample(21),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "emptyFirstName",
    email: faker.internet.email(),
    firstname: "",
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "emptyLastName",
    email: faker.internet.email(),
    firstname: faker.person.firstName(),
    lastname: "",
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "longLastName",
    email: faker.internet.email(),
    firstname: faker.person.firstName(),
    lastname: faker.string.sample(21),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "emptyEmail",
    email: "",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "invalidEmail@",
    email: "invalid.mail.ex",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "invalidEmail@@",
    email: "invalid@@mail.ex",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "invalidEmailDomen",
    email: "invalid@mail",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "alreadyExistEmail",
    email: "test@mail.pw",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "longEmail",
    email: faker.string.sample(38) + "50@example.ex",
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 10 }),
  },
  {
    title: "emptyPassword",
    email: faker.internet.email(),
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: "",
  },
  {
    title: "shortPassword",
    email: faker.internet.email(),
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 6 }),
  },
  {
    title: "longPassword",
    email: faker.internet.email(),
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    password: faker.internet.password({ length: 101 }),
  },
];
