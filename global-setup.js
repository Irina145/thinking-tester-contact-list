const { chromium } = require("@playwright/test");
const { LoginPage } = require("./page_objects/loginPage");


async function updateToken(baseURL, storageState) {
  const browser = await chromium.launch();
  const page = await browser.newPage();
  const Login = new LoginPage(page);
  await page.goto(baseURL);
  await Login.auth(process.env.EMAIL, process.env.PASSWORD);
  await page.waitForTimeout(3000)
  await page.context().storageState({ path: storageState });
  await browser.close();
}

module.exports = async (config) => {

  const { baseURL, storageState} = config.projects[0].use;
  // Инициализация обновления токена
  await updateToken(baseURL, storageState);
};
