const { test, expect } = require("@playwright/test");
const BasePage = require("./basePage");

exports.addContactPage = class AddContactPage extends BasePage {
  /**
   * @param {import('@playwright/test').Page} page
   */
  constructor(page) {
    super(page);
    this.page = page;
    this.fieldFirstName = page.locator('input[id="firstName"]'),
      this.fieldEmail = page.locator("#email"),
      this.fieldLastName = page.locator('input[id="lastName"]'),
      this.fieldBirthDay = page.locator('input[id="birthdate"]'),
      this.fieldPhone = page.locator('input[id="phone"]'),
      this.fieldAdress1 = page.locator('input[id="street1"]'),
      this.fieldAdress2 = page.locator('input[id="street2"]'),
      this.fieldCity = page.locator('input[id="city"]'),
      this.fieldStateProvince = page.locator('input[id="stateProvince"]'),
      this.fieldPostalCode = page.locator('input[id="postalCode"]'),
      this.fieldCountry = page.locator('input[id="country"]'),
      this.fieldPhone = page.locator('input[id="phone"]');
  }

  /**
   * Метод заполняет поле "First Name"
   * @param {string} firstName
   */
  async fillFirstName(firstName) {
    await this.fieldFirstName.fill(firstName);
  }

  /**
   * Метод заполняет поле "Last Name"
   * @param {string} lastName
   */
  async fillLastName(lastName) {
    await this.fieldLastName.fill(lastName);
  }

  /**
   * Метод заполняет поле "Date of Birth"
   * @param {string} bithDate в формате yyyy-mm-dd
   */
  async fillBirthDate(birthdate) {
    await console.log(birthdate);
    await this.fieldBirthDay.fill(birthdate);
  }

  /**
   * Метод заполняет поле "Email"
   * @param {string} email
   */
  async fillEmail(email) {
    await this.fieldEmail.fill(email);
  }

  /**
   * Метод заполняет поле "Phone"
   * @param {string} phone в формате "8005551234"
   */
  async fillPhone(phone) {
    await this.fieldPhone.fill(phone);
  }

  /**
   * Метод заполняет поле "Street Address 1"
   * @param {string} streetAdress
   */
  async fillStreetAdress1(streetAdress) {
    await this.fieldAdress1.fill(streetAdress);
  }

  /**
   * Метод заполняет поле "Street Address 2"
   * @param {string} streetAdress
   */
  async fillStreetAdress2(streetAdress) {
    await this.fieldAdress2.fill(streetAdress);
  }

  /**
   * Метод заполняет поле "City"
   * @param {string} city
   */
  async fillCity(city) {
    await this.fieldCity.fill(city);
  }

  /**
   * Метод заполняет поле "State or Province"
   * @param {string} StateOrProvince
   */
  async fillStateOrProvince(StateOrProvince) {
    await this.fieldStateProvince.fill(StateOrProvince);
  }

  /**
   * Метод заполняет поле "Postal Code"
   * @param {string} postalCode
   */
  async fillPostalCode(postalCode) {
    await this.fieldPostalCode.fill(postalCode);
  }

  /**
   * Метод заполняет поле "Country"
   * @param {string} country
   */
  async fillCountry(country) {
    await this.fieldCountry.fill(country);
  }

  /**
     * 
     * @param {Object} data 
     * @example fillAddContactForm({
        title: string;
        firstname: string;
        lastname: string;
        birthdate?:string;
        email?: string;
        phone?: string;
        streetAdress1?: string;
        streetAdress2?: string;
        city?: string;
        stateOrProvince?: string;
        country?: string;
        postalCode?: string;
     })
     */
  async fillAddContactForm(data) {
    await this.fillFirstName(data.firstname);
    await this.fillLastName(data.lastname);
    if (data.birthdate) {
      await this.fillBirthDate(data.birthdate);
    }
    if (data.email) {
      await this.fillEmail(data.email);
    }
    if (data.phone) {
      await this.fillPhone(data.phone);
    }
    if (data.streetAdress1) {
      await this.fillStreetAdress1(data.streetAdress1);
    }
    if (data.streetAdress2) {
      await this.fillStreetAdress2(data.streetAdress2);
    }
    if (data.city) {
      await this.fillCity(data.city);
    }
    if (data.stateOrProvince) {
      await this.fillStateOrProvince(data.stateOrProvince);
    }
    if (data.postalCode) {
      await this.fillPostalCode(data.postalCode);
    }
    if (data.country) {
      await this.fillCountry(data.country);
    }
  }
};

