const { test, expect } = require("@playwright/test");
const BasePage = require("./basePage");
const { ContactListPage } = require("./contactListPage");

exports.AddUserPage = class AddUserPage extends BasePage {
  /**
   * @param {import('@playwright/test').Page} page
   */
  constructor(page) {
    super(page);
    this.page = page,
      this.titlePage = page.locator('h1[itemprop="name"]'),
      this.fieldFirstName = page.locator('input[id="firstName"]'),
      this.fieldLastName = page.locator('input[id="lastName"]'),
      this.fieldEmail = page.locator('input[id="email"]'),
      this.fieldPassword = page.locator('input[id="password"]'),
      this.errorMessage = page.locator("#error");
  }

  /**
   * Метод заполняет поле "First Name"
   * @param {string} firstName
   */
  async fillFirstName(firstName) {
    if (firstName) {
      await this.fieldFirstName.fill(firstName);
    } else {
      console.log(`${firstName} is empty`);
    }
  }

  /**
   * Метод заполняет поле "Last Name"
   * @param {string} lastName
   */
  async fillLastName(lastName) {
    if (lastName != null) {
      await this.fieldLastName.fill(lastName);
    } else {
      console.log(`${lastName} is empty`);
    }
  }

  /**
   * Метод заполняет поле "Email"
   * @param {string} email
   */
  async fillEmail(email) {
    if (email) {
      await this.fieldEmail.fill(email);
    }
  }

  /**
   * Метод заполняет поле "Password"
   * @param {string} password
   */
  async fillPassword(password) {
    if (password) {
      await this.fieldPassword.fill(password);
    }
  }

  /**
   * Метод отправляет форму регистрации
   */
  async submitForm() {
    await this.page.waitForLoadState()
    await this.btnSubmit.click();
    return new ContactListPage(this.page);
  }
};
