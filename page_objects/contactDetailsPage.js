const { test, expect } = require("@playwright/test");
const BasePage = require("./basePage");
const { EditeContactPage } = require("./editeContactPage");

exports.ContactDetailsPage = class ContactDetailsPage extends BasePage {
  /**
   * @param {import('@playwright/test').Page} page
   */
  constructor(page) {
    super(page);
    this.page = page;
    this.btnEditContact = page.locator('button[id="edit-contact"]'),
      this.btnDeleteContact = page.locator('button[id="delete"]'),
      this.btnEReturn2ContactList = page.locator('button[id="return"]'),
      this.fieldFirstName = page.locator('[id="firstName"]'),
      this.fieldEmail = page.locator("#email"),
      this.fieldLastName = page.locator('[id="lastName"]'),
      this.fieldBirthDay = page.locator('[id="birthdate"]'),
      this.fieldPhone = page.locator('[id="phone"]'),
      this.fieldAdress1 = page.locator('[id="street1"]'),
      this.fieldAdress2 = page.locator('[id="street2"]'),
      this.fieldCity = page.locator('[id="city"]'),
      this.fieldStateProvince = page.locator('[id="stateProvince"]'),
      this.fieldPostalCode = page.locator('[id="postalCode"]'),
      this.fieldCountry = page.locator('[id="country"]'),
      this.fieldPhone = page.locator('[id="phone"]');
  }

  /**
   * Метод возвращает значение "First Name"
   */
  async getFirstName() {
    return await this.fieldFirstName.innerText();
  }

  /**
   * Метод возвращает значение "Last Name"
   */
  async getLastName() {
    return await this.fieldLastName.innerText();
  }

  /**
   * Метод возвращает значение "Date of Birth"
   */
  async getBirthDate() {
    return await this.fieldBirthDay.innerText();
  }

  /**
   * Метод возвращает значение "Email"
   */
  async getEmail() {
    return await this.fieldEmail.innerText();
  }

  /**
   * Метод возвращает значение "Phone"
   */
  async getPhone() {
    return await this.fieldPhone.innerText();
  }

  /**
   * Метод возвращает значение "Street Address 1"
   */
  async getStreet1() {
    return await this.fieldAdress1.innerText();
  }

  /**
   * Метод возвращает значение "Street Address 2"
   */
  async getStreet2() {
    return await this.fieldAdress2.innerText();
  }

  /**
   * Метод возвращает значение "City"
   */
  async getCity() {
    return await this.fieldCity.innerText();
  }

  /**
   * Метод возвращает значение "State or Province"
   */
  async getStateProvince() {
    return await this.fieldStateProvince.innerText();
  }

  /**
   * Метод возвращает значение "Postal Code"
   */
  async getPostalCode() {
    return await this.fieldPostalCode.innerText();
  }

  /**
   * Метод возвращает значение "Countru"
   */
  async getCountry() {
    return await this.fieldCountry.innerText();
  }

  /**
   * Метод подтверждает или отменяет удаленеи контакта в алерте
   * @param {string} confirm 'accept' или 'dismiss'
   */
  async confirmAlertDelete(confirm) {
    await this.page.on("dialog", async (dialog) => {
      if (dialog.message() == "Are you sure you want to delete this contact?") {
        if (confirm == "accept") {
          await dialog.accept();
        }
        if (confirm == "dismiss") {
          await dialog.dismiss();
        }
      }
    });
  }

  async openEditForm() {
    await this.btnEditContact.click();
    await this.page.waitForURL("**/editContact");
    await this.page.waitForLoadState();
    return new EditeContactPage(this.page);
  }

  async openContactList() {
    await this.btnEReturn2ContactList.click();
  }
};
