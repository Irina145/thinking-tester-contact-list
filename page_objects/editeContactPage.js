const { test, expect } = require("@playwright/test");
const BasePage = require("./basePage");

exports.EditeContactPage = class EditeContactPage extends BasePage {
  /**
   * @param {import('@playwright/test').Page} page
   */
  constructor(page) {
    super(page);
    this.page = page;
    this.fieldFirstName = page.locator('input[id="firstName"]'),
      this.fieldLastName = page.locator('input[id="lastName"]'),
      this.fieldBirthDay = page.locator('input[id="birthdate"]'),
      this.fieldEmail = page.locator("#email"),
      this.fieldPhone = page.locator('input[id="phone"]'),
      this.fieldAdress1 = page.locator('input[id="street1"]'),
      this.fieldAdress2 = page.locator('input[id="street2"]'),
      this.fieldCity = page.locator('input[id="city"]'),
      this.fieldStateProvince = page.locator('input[id="stateProvince"]'),
      this.fieldPostalCode = page.locator('input[id="postalCode"]'),
      this.fieldCountry = page.locator('input[id="country"]'),
      this.fieldPhone = page.locator('input[id="phone"]');
  }


  /**
   * Метод заполняет поле "First Name"
   * @param {string} firstName
   */
  async editeFirstName(firstName) {
    await this.fieldFirstName.clear();
    await this.fieldFirstName.fill(firstName);
  }

  /**
   * Метод заполняет поле "Last Name"
   * @param {string} lastName
   */
  async editeLastName(lastName) {
    await this.fieldLastName.clear();
    await this.fieldLastName.fill(lastName);
  }

  /**
   * Метод заполняет поле "Date of Birth"
   * @param {string} bithDate в формате yyyy-mm-dd
   */
  async editeBirthDate(birthdate) {
    await this.fieldBirthDay.clear();
    await this.fieldBirthDay.fill(birthdate);
  }

  /**
   * Метод заполняет поле "Email"
   * @param {string} email
   */
  async editeEmail(email) {
    await this.fieldEmail.clear();
    await this.fieldEmail.fill(email);
  }

  /**
   * Метод заполняет поле "Phone"
   * @param {string} phone в формате "8005551234"
   */
  async editePhone(phone) {
    await this.fieldPhone.clear();
    await this.fieldPhone.fill(phone);
  }

  /**
   * Метод заполняет поле "Street Address 1"
   * @param {string} streetAdress
   */
  async editeStreetAdress1(streetAdress) {
    await this.fieldAdress1.clear();
    await this.fieldAdress1.fill(streetAdress);
  }

  /**
   * Метод заполняет поле "Street Address 2"
   * @param {string} streetAdress
   */
  async editeStreetAdress2(streetAdress) {
    await this.fieldAdress2.clear();
    await this.fieldAdress2.fill(streetAdress);
  }

  /**
   * Метод заполняет поле "City"
   * @param {string} city
   */
  async editeCity(city) {
    await this.fieldCity.clear();
    await this.fieldCity.fill(city);
  }

  /**
   * Метод заполняет поле "State or Province"
   * @param {string} StateOrProvince
   */
  async editeStateOrProvince(StateOrProvince) {
    await this.fieldStateProvince.clear();
    await this.fieldStateProvince.fill(StateOrProvince);
  }

  /**
   * Метод возвращает заполняет поле "Postal Code"
   * @param {string} postalCode
   */
  async editePostalCode(postalCode) {
    await this.fieldPostalCode.clear();
    await this.fieldPostalCode.fill(postalCode);
  }

  /**
   * Метод заполняет поле "Country"
   * @param {string} country
   */
  async editeCountry(country) {
    await this.fieldCountry.clear();
    await this.fieldCountry.fill(country);
  }

  async editeContactForm(data) {
    await this.editeFirstName(data.firstname);
    await this.editeLastName(data.lastname);
    await this.editeBirthDate(data.birthdate);
    await this.editeEmail(data.email);
    await this.editePhone(data.phone);
    await this.editeStreetAdress1(data.streetAdress1);
    await this.editeStreetAdress2(data.streetAdress2);
    await this.editeCity(data.city);
    await this.editeStateOrProvince(data.stateOrProvince);
    await this.editePostalCode(data.postalCode);
    await this.editeCountry(data.country);
  }
};
