const { test, expect } = require("@playwright/test");
const BasePage = require("./basePage");
const { ContactListPage } = require("./contactListPage");
const { AddUserPage } = require("./addUserPage");

// class LoginPage extends BasePage {
exports.LoginPage = class LoginPage extends BasePage {
  /**
   * @param {import('@playwright/test').Page} page
   */
  constructor(page) {
    super(page);
    this.page = page;
    this.fieldLogin = page.locator("input#email"),
      this.fieldPassword = page.locator('input[id="password"]'),
      this.btnSignUp = page.locator('button[id="signup"]'),
      this.welcomeMessage = page.locator("div.welcome-message");
  }

  /**
   * Метод выполняет переход по указанному url
   * @param {string} url
   */
  async open(url = "/") {
    await this.page.goto(url);
  }

  /**
   * Метод заполняет поле "Log In"
   * @param {string} login
   */
  async fillLogin(login) {
    await this.fieldLogin.fill(login);
  }

  /**
   * Метод заполняет поле "Password"
   * @param {string} password
   */
  async fillPassword(password) {
    await this.fieldPassword.fill(password);
  }

  /**
   * Метод принимает логин и пароль пользователя. Заполняет форму авторизации и отправляет ее
   * @param {string} login
   * @param {string} password
   */
  async auth(login, password) {
    await this.fillLogin(login);
    await this.fillPassword(password);
    await this.submitForm();
  }

  /**
   * Метод нажимает на кнопку "SignUp" и переходит к странице регистрации нового пользователя
   */
  async goToSignupPage() {
    await this.btnSignUp.click();
    return new AddUserPage(this.page);
  }

  /**
   * Метод отправляет форму авторизации
   */
  async submitForm() {
    await this.btnSubmit.click();
    return new ContactListPage(this.page);
  }
};

