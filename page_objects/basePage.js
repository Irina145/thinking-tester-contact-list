const { test, expect } = require("@playwright/test");

class BasePage {
  /**
   * @param {import('@playwright/test').Page} page
   */
  constructor(page) {
    this.page = page,
      this.headerPage = page.getByRole("heading"),
      this.btnSubmit = page.locator('button[id="submit"]'),
      this.btnCancel = page.locator('button[id="cancel"]'),
      this.btnLogout = page.locator('button[id="logout"]'),
      this.errorMessage = page.locator("#error");
  }

  /**
   * Метод переходит по url
   * @param {url} url
   */
  async goto(url) {
    await this.page.goto(url);
  }

  /**
   * Метод нажимает на кнопку "Submit" и отправляет форму
   */
  async submitForm() {
    await this.btnSubmit.click();
  }

  /**
   * Метод нажимает на кнопку "Cancel" и отменяет отправку формы (возврат на родительскую страницу)
   */
  async cancelForm() {
    await this.btnCancel.click();
  }

  async userLogOut() {
    await this.btnLogout.click();
  }

  async getErrorMessage() {
    return await this.errorMessage.innerText();
  }
}

module.exports = BasePage;
