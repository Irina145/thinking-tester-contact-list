const { test, expect } = require("@playwright/test");
const BasePage = require("./basePage");
const { addContactPage } = require("./addContactPage");
const { ContactDetailsPage } = require("./contactDetailsPage");

exports.ContactListPage = class ContactListPage extends BasePage {
  /**
   * @param {import('@playwright/test').Page} page
   */
  constructor(page) {
    super(page);
    this.page = page;
    this.btnAddNewContact = page.locator('button[id="add-contact"]'),
      this.tableContacts = page.locator('table[id="myTable"]'),
      this.rowTable = page.locator("tr.contactTableBodyRow"),
      this.cellUuid = page.locator("td:nth-child(1)"),
      this.cellFullname = page.locator("td:nth-child(2)"),
      this.cellBirthday = page.locator("td:nth-child(3)"),
      this.cellEmail = page.locator("td:nth-child(4)"),
      this.cellPhone = page.locator("td:nth-child(5)"),
      this.cellAdress = page.locator("td:nth-child(6)"),
      this.cellCityStatePostalCode = page.locator("td:nth-child(7)"),
      this.cellCountry = page.locator("td:nth-child(8)");
  }

  /**
   * Метод переходит по url
   * @param {url} url
   */
  async open() {
    await this.page.goto("/contactList");
    await this.page.waitForURL("**/contactList");
    await this.page.waitForLoadState();
  }

  /**
   * Метод нажимает на кнопку "Add Contact" и переходит к странице добавления нового контакта
   */
  async goToAddContactPage() {
    await this.btnAddNewContact.click();
    return new addContactPage(this.page);
  }

  /**
   * Метод возвращает список ячеек колонки Name
   * @returns
   */
  async getCellFullnameList() {
    return await this.cellFullname.all();
  }

  /**
   * Метод возвращает uuid по полному имени контакта. Если контактов несколько, вернет uuid первого такого контакта
   * @param {string} fullName
   * @returns uuid
   */
  async getContactUuid(fullName) {
    const rows = await this.page.locator("tr.contactTableBodyRow").all();
    for (const row of rows) {
      const cellText = await row.locator("td:nth-child(2)").textContent();
      if (cellText === fullName) {
        const uuid = await row
          .locator('td[hidden="true"]')
          .first()
          .textContent();
        return uuid;
      }
    }
  }

  /**
   * Метод открывает карточку контакта по uuid
   * @param {string} rowUuid
   * @returns
   */
  async openContactCardByUuid(rowUuid) {
    await this.rowTable.filter({ hasText: String(rowUuid) }).click();
    await this.page.waitForURL("**/contactDetails");
    await this.page.waitForLoadState();
    return new ContactDetailsPage(this.page);
  }

  /**
   * Метод получает значение ячейки Name
   * @param {string} rowUuid
   * @returns
   */
  async getCellFullname(rowUuid) {
    return await this.rowTable
      .filter({ hasText: String(rowUuid) })
      .locator(`td:nth-child(2)`)
      .textContent();
  }

  /**
   * Метод получает значение ячейки Birthdate
   * @param {string} rowUuid
   * @returns
   */
  async getLocatorCellBirtday(rowUuid) {
    return await this.rowTable
      .filter({ hasText: String(rowUuid) })
      .locator(`td:nth-child(3)`)
      .textContent();
  }

  /**
   * Метод получает значение ячейки Email
   * @param {string} rowUuid
   * @returns
   */
  async getLocatorCellEmail(rowUuid) {
    return await this.rowTable
      .filter({ hasText: String(rowUuid) })
      .locator(`td:nth-child(4)`)
      .textContent();
  }

  /**
   * Метод получает значение ячейки Phone
   * @param {string} rowUuid
   * @returns
   */
  async getLocatorCellPhone(rowUuid) {
    return await this.rowTable
      .filter({ hasText: String(rowUuid) })
      .locator(`td:nth-child(5)`)
      .textContent();
  }

  /**
   * Метод получает значение ячейки Address
   * @param {string} rowUuid
   * @returns
   */
  async getLocatorCellAdress(rowUuid) {
    return await this.rowTable
      .filter({ hasText: String(rowUuid) })
      .locator(`td:nth-child(6)`)
      .textContent();
  }

  /**
   * Метод получает значение ячейки City, State/Province, Postal Code
   * @param {string} rowUuid
   * @returns
   */
  async getLocatorCellCityStatePostal(rowUuid) {
    return await this.rowTable
      .filter({ hasText: String(rowUuid) })
      .locator(`td:nth-child(7)`)
      .textContent();
  }

  /**
   * Метод получает значение ячейки Country
   * @param {string} rowUuid
   * @returns
   */
  async getLocatorCellCountry(rowUuid) {
    return await this.rowTable
      .filter({ hasText: String(rowUuid) })
      .locator(`td:nth-child(8)`)
      .textContent();
  }
};
