const { test, expect } = require("@playwright/test");
const supertest = require("supertest");
const fs = require("fs");

exports.ContactAPI = class ContactAPI {
  /**
   * @param {import('@playwright/test').Page} page
   */
  constructor(page) {
    this.page = page;
  }

  /**
   * Метод удаляет контакт
   * @returns response
   */
  async deletededContact(uuid) {
    // Чтение файла state.json
    const fileData = await fs.readFileSync("state.json", "utf8");
    const state = await JSON.parse(fileData);
    const token = await state.cookies[0].value;
    await console.log(uuid);
    //Запрос на удаление контакта
    const response = await supertest(`${process.env.BASEURL}`)

      .delete(`/contacts/${uuid}`)
      .set("Authorization", `Bearer ${token}`);

    console.log(JSON.stringify(response.status), "DELETED");
    return {
      headers: response.headers,
      status: response.status,
    };
  }

  /**
   * Метод создает контакт
   * @returns response
   */
  async createdContact(data) {
    // Чтение файла state.json
    const fileData = await fs.readFileSync("state.json", "utf8");
    const state = await JSON.parse(fileData);
    const token = await state.cookies[0].value;

    //Запрос на cоздание контакта
    const response = await supertest(`${process.env.BASEURL}`)
      .post(`/contacts/`)
      .set("Authorization", `Bearer ${token}`)
      .send(data);

    console.log(JSON.stringify(response.status), "CREATED");
    return {
      headers: response.headers,
      status: response.status,
      uuid: response.body["_id"],
    };
  }
};
