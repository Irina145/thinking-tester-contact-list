const { test, expect } = require("@playwright/test");
const supertest = require("supertest");
const fs = require("fs");


exports.UserAPI = class UserAPI {
  /**
   * @param {import('@playwright/test').Page} page
   */
  constructor(page) {
    this.page = page;
  }

/**
   * Метод удаляет пользователя
   * @returns response
   */
async authUser(data) {
  const response = await supertest(`${process.env.BASEURL}`)
    .post(`/users/login`)
    .send(data)

  console.log(JSON.stringify(response.status), "LOGIN");
  return {
    headers: response.headers,
    status: response.status,
    token: response.body.token
  };
}


  /**
   * Метод удаляет пользователя
   * @returns response
   */
  async deletededUser(token) {
    const response = await supertest(`${process.env.BASEURL}`)
      .delete(`/users/me`)
      .set("Authorization", `Bearer ${token}`);

    console.log(JSON.stringify(response.status), "DELETED");
    return {
      headers: response.headers,
      status: response.status,
    };
  }
};

