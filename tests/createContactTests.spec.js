// @ts-check
const { test, expect, defineConfig } = require("@playwright/test");
const { ContactListPage } = require("../page_objects/contactListPage");
const { ContactAPI } = require("../apiHelper/contactAPI");
const { contactData, invalidContacttData, generationErrorText } = require("../fixtures/contact");

test.describe(`Позитивные сценарии`, async () => {
  const contactDataList = contactData
  let fullname = ''

  test.beforeEach(async ({ page }) => {
    const ContactList = new ContactListPage(page)
    await ContactList.open()
    //Ожидание завершения запроса на получение спиcка контактов
    await expect(ContactList.headerPage).toHaveText('Contact List')
  });

  test.afterEach(async ({ page }) => {
    //удаление контакта после теста
    const ContactList = new ContactListPage(page)
    const uuid = await ContactList.getContactUuid(fullname)
    const apiContact = new ContactAPI(page)
    await apiContact.deletededContact(uuid)
  });

  for (const data of contactDataList) {

    test(`Добавление контакта успешно: ${data.title}`, {
      tag: ['@all','@contact', '@add'],
    }, async ({ page }) => {
      const ContactList = new ContactListPage(page)
      const AddContact = await ContactList.goToAddContactPage()
      fullname = `${data.firstname} ${data.lastname}`

      //Ожидание перехода к анкете
      await page.waitForURL('**\/addContact')
      await page.waitForLoadState()

      // Заполнение и отправка формы
      await AddContact.fillAddContactForm(data)
      await AddContact.submitForm()
      const response = await page.waitForResponse(`**\/contacts`);
      expect(response.status()).toBe(201)

      await page.waitForURL('**\/contactList')
      await page.waitForResponse(`**\/contacts`);
      await page.waitForLoadState()

      //Проверка, что контакт появился в таблице
      const uuid = await ContactList.getContactUuid(fullname)
      expect(await ContactList.getCellFullname(String(uuid))).toContain(fullname)
      expect(await ContactList.getLocatorCellBirtday(String(uuid))).toContain(data.birthdate)
      expect(await ContactList.getLocatorCellEmail(String(uuid))).toContain((data.email).toLowerCase())
      expect(await ContactList.getLocatorCellPhone(String(uuid))).toContain(data.phone)
      expect(await ContactList.getLocatorCellAdress(String(uuid))).toContain(data.streetAdress1)
      expect(await ContactList.getLocatorCellAdress(String(uuid))).toContain(data.streetAdress2)
      expect(await ContactList.getLocatorCellCityStatePostal(String(uuid))).toContain(data.city)
      expect(await ContactList.getLocatorCellCityStatePostal(String(uuid))).toContain(data.stateOrProvince)
      expect(await ContactList.getLocatorCellCityStatePostal(String(uuid))).toContain(data.postalCode)
      expect(await ContactList.getLocatorCellCountry(String(uuid))).toContain(data.country)
    });
  }
})

test.describe(`Негативные сценарии`, async () => {
  let fullname = ''

  test.beforeEach(async ({ page }) => {
    const ContactList = new ContactListPage(page)
    await ContactList.open()
    await expect(ContactList.headerPage).toHaveText('Contact List')
  });

  for (const invalid of invalidContacttData) {

    test(`Невозможность добавления контакта с невалидными данными: ${invalid.title}`, {
      tag: ['@all','@contact', '@add', '@negative'],
    }, async ({ page }) => {
      const ContactList = new ContactListPage(page)
      const AddContact = await ContactList.goToAddContactPage()
      const errorMessage = await generationErrorText(invalid)

      fullname = `${invalid.firstname} ${invalid.lastname}`

      //Ожидание перехода к анкете
      await page.waitForURL('**\/addContact')
      await page.waitForLoadState()

      // Заполнение и отправка формы
      await AddContact.fillAddContactForm(invalid)
      await AddContact.submitForm()

      //Переход на страницу списка контактов не произошел
      await expect(AddContact.headerPage).toHaveText('Add Contact')

      //Ожидание завершения с ошибкой запроса на создание контакта
      const response = await page.waitForResponse(`**\/contacts`);
      expect(response.status()).toBe(400)

      //Проверка соотвествия фактического текста ошибки и ожидаемого
      const errorMessageText = await AddContact.getErrorMessage()
      await expect(errorMessageText).toContain(errorMessage)


      //Проверка, что контакта с таким именем нет в таблице
      await ContactList.open()

      for (const contact of await ContactList.cellFullname.all()) {
        let fullNameText = await contact.innerText()
        await expect(fullNameText).not.toContain(fullname)
      }
    });
  }
})
