// @ts-check
const { test, expect } = require("@playwright/test");
const { ContactListPage } = require("../page_objects/contactListPage");
const { ContactAPI } = require("../apiHelper/contactAPI");
const { contactData, invalidContacttData, generationErrorText, contactDataForEdite } = require("../fixtures/editeContact");
const { ContactDetailsPage } = require("../page_objects/contactDetailsPage");

test.describe(`Позитивные сценарии`, async () => {
    const contactDataList = contactData
    let fullname = ''
    let uuid = ''
    const editeDate = contactDataForEdite

    test.beforeEach(async ({ page }) => {
        //Создание контакта для теста
        const apiContact = new ContactAPI(page)
        const response = await apiContact.createdContact({
            "firstName": editeDate.firstname,
            "lastName": editeDate.lastname,
            "birthdate": editeDate.birthdate,
            "email": editeDate.email,
            "phone": editeDate.phone,
            "street1": editeDate.streetAdress1,
            "street2": editeDate.streetAdress2,
            "city": editeDate.city,
            "stateProvince": editeDate.stateOrProvince,
            "postalCode": editeDate.postalCode,
            "country": editeDate.country
        })
        uuid = await response.uuid

        const ContactList = new ContactListPage(page)
        await ContactList.open()
        await expect(ContactList.headerPage).toHaveText('Contact List')

        await ContactList.openContactCardByUuid(uuid)
    });

    test.afterEach(async ({ page }) => {
        //удаление контакта после теста
        const apiContact = new ContactAPI(page)
        await apiContact.deletededContact(uuid)
    });

    for (const data of contactDataList) {

        test(`Редактирование контакта успешно: ${data.title}`, {
            tag: ['@all','@contact', '@edite'],
        }, async ({ page }) => {
            await console.log(data)
            const ContactList = new ContactListPage(page)
            const ContactCard = new ContactDetailsPage(page)

            const EditeForm = await ContactCard.openEditForm()
            await page.waitForResponse(`**\/contacts/${uuid}`);

            // Заполнение и отправка формы
            await EditeForm.editeContactForm(data)
            await EditeForm.submitForm()

            //Ожидание перехода к карточке контакта
            await page.waitForURL('**\/contactDetails')
            await page.waitForLoadState()
            await page.waitForResponse(`**\/contacts/${uuid}`);

            //Проверка, что в карточке контакта данные обновлены
            expect(await ContactCard.getFirstName()).toContain(data.firstname)
            expect(await ContactCard.getLastName()).toContain(data.lastname)
            expect(await ContactCard.getBirthDate()).toContain(data.birthdate)
            expect(await ContactCard.getEmail()).toContain((data.email).toLowerCase())
            expect(await ContactCard.getPhone()).toContain(data.phone)
            expect(await ContactCard.getStreet1()).toContain(data.streetAdress1)
            expect(await ContactCard.getStreet2()).toContain(data.streetAdress2)
            expect(await ContactCard.getCity()).toContain(data.city)
            expect(await ContactCard.getStateProvince()).toContain(data.stateOrProvince)
            expect(await ContactCard.getPostalCode()).toContain(data.postalCode)
            expect(await ContactCard.getCountry()).toContain(data.country)

            //Переход к списку контактов
            await ContactList.open()
            await page.waitForResponse(`**\/contacts`);


            //Проверка, что контакт обновился в таблице
            expect(await ContactList.getCellFullname(String(uuid))).toContain(fullname)
            expect(await ContactList.getLocatorCellBirtday(String(uuid))).toContain(data.birthdate)
            expect(await ContactList.getLocatorCellEmail(String(uuid))).toContain((data.email).toLowerCase())
            expect(await ContactList.getLocatorCellPhone(String(uuid))).toContain(data.phone)
            expect(await ContactList.getLocatorCellAdress(String(uuid))).toContain(data.streetAdress1)
            expect(await ContactList.getLocatorCellAdress(String(uuid))).toContain(data.streetAdress2)
            expect(await ContactList.getLocatorCellCityStatePostal(String(uuid))).toContain(data.city)
            expect(await ContactList.getLocatorCellCityStatePostal(String(uuid))).toContain(data.stateOrProvince)
            expect(await ContactList.getLocatorCellCityStatePostal(String(uuid))).toContain(data.postalCode)
            expect(await ContactList.getLocatorCellCountry(String(uuid))).toContain(data.country)
        });
    }
})


test.describe(`Негативные сценарии`, async () => {
    let fullname = ''
    let uuid = ''
    const editeDate = contactDataForEdite

    test.beforeEach(async ({ page }) => {
        //Создание контакта для теста
        const apiContact = new ContactAPI(page)
        const response = await apiContact.createdContact({
            "firstName": editeDate.firstname,
            "lastName": editeDate.lastname,
            "birthdate": editeDate.birthdate,
            "email": editeDate.email,
            "phone": editeDate.phone,
            "street1": editeDate.streetAdress1,
            "street2": editeDate.streetAdress2,
            "city": editeDate.city,
            "stateProvince": editeDate.stateOrProvince,
            "postalCode": editeDate.postalCode,
            "country": editeDate.country
        })
        uuid = await response.uuid
        fullname = `${editeDate.firstname} ${editeDate.lastname}`

        const ContactList = new ContactListPage(page)
        await ContactList.open()
        await expect(ContactList.headerPage).toHaveText('Contact List')

        await ContactList.openContactCardByUuid(uuid)
    });

    test.afterEach(async ({ page }) => {
        //удаление контакта после теста
        const apiContact = new ContactAPI(page)
        await apiContact.deletededContact(uuid)
    });

    for (const invaid of invalidContacttData) {

        test(`Невозможность редактирования контакта с невалидными данными: ${invaid.title}`, {
            tag: ['@all','@contact', '@edite', '@negative'],
        }, async ({ page }) => {
            await console.log(invaid)
            const ContactList = new ContactListPage(page)
            const ContactCard = new ContactDetailsPage(page)
            const errorMessage = await generationErrorText(invaid)

            const EditeForm = await ContactCard.openEditForm()
            await page.waitForResponse(`**\/contacts/${uuid}`);


            // Заполнение и отправка формы
            await EditeForm.editeContactForm(invaid)
            await EditeForm.submitForm()

            //Переход на страницу списка контактов не произошел
            await expect(EditeForm.headerPage).toHaveText('Edit Contact')

            //Ожидание завершения с ошибкой запроса на создание контакта
            const response = await page.waitForResponse(`**\/contacts/${uuid}`);
            expect(response.status()).toBe(400)

            //Проверка соотвествия фактического текста ошибки и ожидаемого
            const errorMessageText = await EditeForm.getErrorMessage()
            await expect(errorMessageText).toContain(errorMessage)

            //Проверка, что данные контакта в таблице не изменились на невалидные
            await ContactList.open()

            expect(await ContactList.getCellFullname(String(uuid))).toContain(fullname)
            expect(await ContactList.getLocatorCellBirtday(String(uuid))).toContain(editeDate.birthdate)
            expect(await ContactList.getLocatorCellEmail(String(uuid))).toContain((editeDate.email).toLowerCase())
            expect(await ContactList.getLocatorCellPhone(String(uuid))).toContain(editeDate.phone)
            expect(await ContactList.getLocatorCellAdress(String(uuid))).toContain(editeDate.streetAdress1)
            expect(await ContactList.getLocatorCellAdress(String(uuid))).toContain(editeDate.streetAdress2)
            expect(await ContactList.getLocatorCellCityStatePostal(String(uuid))).toContain(editeDate.city)
            expect(await ContactList.getLocatorCellCityStatePostal(String(uuid))).toContain(editeDate.stateOrProvince)
            expect(await ContactList.getLocatorCellCityStatePostal(String(uuid))).toContain(editeDate.postalCode)
            expect(await ContactList.getLocatorCellCountry(String(uuid))).toContain(editeDate.country)

            //Ожидание перехода к карточке контакта
            await ContactList.openContactCardByUuid(uuid)
            // await page.waitForResponse(`${process.env.BASEURL}/contacts/${uuid}`)
            await page.waitForResponse(`**\/contacts/${uuid}`);

            //Проверка, что в карточке контакта данные не изменились
            expect(await ContactCard.getFirstName()).toContain(editeDate.firstname)
            expect(await ContactCard.getLastName()).toContain(editeDate.lastname)
            expect(await ContactCard.getBirthDate()).toContain(editeDate.birthdate)
            expect(await ContactCard.getEmail()).toContain((editeDate.email).toLowerCase())
            expect(await ContactCard.getPhone()).toContain(editeDate.phone)
            expect(await ContactCard.getStreet1()).toContain(editeDate.streetAdress1)
            expect(await ContactCard.getStreet2()).toContain(editeDate.streetAdress2)
            expect(await ContactCard.getCity()).toContain(editeDate.city)
            expect(await ContactCard.getStateProvince()).toContain(editeDate.stateOrProvince)
            expect(await ContactCard.getPostalCode()).toContain(editeDate.postalCode)
            expect(await ContactCard.getCountry()).toContain(editeDate.country)
        });
    }
})
