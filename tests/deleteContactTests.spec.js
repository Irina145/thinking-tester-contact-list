// @ts-check
const { test, expect } = require("@playwright/test");
const { ContactListPage } = require("../page_objects/contactListPage");
const { ContactAPI } = require("../apiHelper/contactAPI");

test.describe(`Позитивные сценарии`, async () => {
  let uuid = "";
  test.beforeEach(async ({ page }) => {
    //Создание контакта для теста
    const apiContact = new ContactAPI(page);
    const response = await apiContact.createdContact({
      firstName: "John",
      lastName: "Doe",
      birthdate: "1970-01-01",
      email: "jdoe@fake.com",
      phone: "8005555555",
      street1: "1 Main St.",
      street2: "Apartment A",
      city: "Anytown",
      stateProvince: "KS",
      postalCode: "12345",
      country: "USA",
    });
    uuid = await response.uuid;

    const ContactList = new ContactListPage(page);
    await ContactList.open();
  });

  test(`Удаление контакта с подтверждением в алерте успешно`, {
    tag: ['@all',"@contact", '@delete']
  }, async ({ page }) => {
    const ContactList = new ContactListPage(page);

    const ContactCard = await ContactList.openContactCardByUuid(uuid);

    //Ожидание завершения запроса на получение спиcка контактов
    await page.waitForResponse(`**\/contacts/${uuid}`);
    await page.waitForLoadState();
    await expect(ContactCard.headerPage).toHaveText("Contact Details");

    // Обработка диалогов и подтверждение удаления в алерте
    await ContactCard.confirmAlertDelete("accept");
    //Удаление контакта
    await ContactCard.btnDeleteContact.click();

    //Ожидание завершения запроса на удаление контакта
    await page.waitForResponse(`**\/contacts/${uuid}`);
    await page.waitForLoadState();
    await expect(ContactCard.headerPage).toHaveText("Contact List");

    //Проверка, что контакт удален из таблицы
    for (const contact of await ContactList.cellUuid.all()) {
      let UuidText = await contact.innerText();
      await expect(UuidText).not.toContain(uuid);
    }
  },
  );

  test(`Отмена удаления контакта с отказом в алерте`, {
    tag: ['@all',"@contact", '@delete']
  }, async ({ page }) => {
    const ContactList = new ContactListPage(page);

    const ContactCard = await ContactList.openContactCardByUuid(uuid);

    //Ожидание завершения запроса на получение карточки контакта
    await page.waitForResponse(`**\/contacts/${uuid}`);
    await page.waitForLoadState();
    await expect(ContactCard.headerPage).toHaveText("Contact Details");

    // Обработка диалогов и отмена удаления в алерте
    await ContactCard.confirmAlertDelete("dismiss");
    //Попытка удаления контакта
    await ContactCard.btnDeleteContact.click();

    //Проверка, что уход из карточки контакта не выполнен
    await expect(ContactCard.headerPage).toHaveText("Contact Details");

    //Переход к списику контактов
    await ContactList.open();
    // await page.waitForResponse(`${process.env.BASEURL}/contacts`);
    await page.waitForResponse(`**\/contacts`);

    //Проверка, что контакт не удален из таблицы
    expect(await ContactList.getCellFullname(uuid)).toBeTruthy();
  },
  );
});
