// @ts-check
const { test, expect } = require("@playwright/test");
const { registrationData, invalidRegistrationData, generationErrorText, } = require("../fixtures/registration");
const { LoginPage } = require("../page_objects/loginPage");
const { UserAPI } = require("../apiHelper/userAPI");

test.describe(`Позитивные сценарии`, async () => {
  test.use({ storageState: { cookies: [], origins: [] } });
  test.afterEach(async ({ page }) => {
    const token = (await page.context().storageState()).cookies[0].value
    const apiUser = new UserAPI(page);
    await apiUser.deletededUser(token);
  });
  for (const data of registrationData) {
    test(
      `Регистрация пользователя успешно: ${data.title}`,
      {
        tag: ['@all',"@registration", "@user"],
      },
      async ({ page }) => {
        const Login = new LoginPage(page);
        await console.log(data)

        await Login.open("/login");
        await expect(Login.headerPage).toHaveText("Contact List App");

        //Переход на страницу регистрации
        const Registration = await Login.goToSignupPage();
        await expect(Registration.headerPage).toHaveText("Add User");

        //Заполнение и отправка формы
        await Registration.fillFirstName(data.firstname);
        await Registration.fillLastName(data.lastname);
        await Registration.fillEmail(data.email);
        await Registration.fillPassword(data.password);
        const App = await Registration.submitForm();

        //Ожидание завершения с кодом 200 запроса на регистрацию
        const response = await page.waitForResponse(`**/users`)  
        expect(await response.status()).toBe(201)

        //Проверка, что вход в приложение выполнен
        expect(App.headerPage).toHaveText("Contact List");
      },
    );
  }
});

test.describe(`Негативные сценарии`, async () => {
  test.use({ storageState: { cookies: [], origins: [] } });

  for (const invalid of invalidRegistrationData) {
    test(
      `Невозможность регистрации с невалидными данными: ${invalid.title}`,
      {
        tag: ['@all',"@registration", "@user", "@negative"],
      },
      async ({ page }) => {
        const Login = new LoginPage(page);
        const errorMessage = await generationErrorText(invalid);

        await Login.open("/login");
        await expect(Login.headerPage).toHaveText("Contact List App");

        //Переход на страницу регистрации
        const Registration = await Login.goToSignupPage();
        await expect(Registration.headerPage).toHaveText("Add User");

        //Заполнение и отправка формы
        await Registration.fillFirstName(invalid.firstname);
        await Registration.fillLastName(invalid.lastname);
        await Registration.fillEmail(invalid.email);
        await Registration.fillPassword(invalid.password);
        await Registration.submitForm();

        //Переход на страницу регистрации не произошел
        await expect(Registration.headerPage).toHaveText("Add User");

        //Ожидание завершения с ошибкой запроса на регистрации
        const response = await page.waitForResponse(`**/users`)  
        expect(await response.status()).toBe(400)

        await page.waitForLoadState();

        //Проверка соотвествия фактического текста ошибки и ожидаемого
        const errorMessageText = await Registration.getErrorMessage();
        await expect(errorMessageText).toContain(errorMessage);
      },
    );
  }
});
