// @ts-check
const { test, expect, defineConfig } = require("@playwright/test");
const { invalidAuthData, generationErrorText } = require("../fixtures/auth");
const { LoginPage } = require("../page_objects/loginPage");

test.describe(`Позитивные сценарии`, async () => {
  test.use({ storageState: { cookies: [], origins: [] } });
  test(
    `Авторизация пользователя успешно`,
    {
      tag: ['@all', "@auth", "@user"],
    },
    async ({ page }) => {
      const Login = new LoginPage(page);

      await Login.open("/login");
      await expect(Login.headerPage).toHaveText("Contact List App");

      //Заполнение и отправка формы
      await Login.fillLogin(`${process.env.EMAIL}`);
      await Login.fillPassword(`${process.env.PASSWORD}`);
      const App = await Login.submitForm();

      //Ожидание завершения с кодом 200 запроса на авторизацию
      const response = await page.waitForResponse(`**\/users/login`);
      await expect(response.status()).toBe(200)

      await page.waitForURL("**/contactList");
      await page.waitForLoadState();

      //Проверка, что вход в приложение выполнен
      await expect(App.headerPage).toHaveText("Contact List");
    },
  );
});

test.describe(`Негативные сценарии`, async () => {
  test.use({ storageState: { cookies: [], origins: [] } });
  for (const invalid of invalidAuthData) {
    test(
      `Невозможность авторизации с невалидными данными: ${invalid.title}`,
      {
        tag: ['@all', "@auth", "@user", "@negative"],
      },
      async ({ page }) => {
        const Login = new LoginPage(page);
        const errorMessage = await generationErrorText(invalid);

        await Login.open("/login");
        await expect(Login.headerPage).toHaveText("Contact List App");

        //Заполнение и отправка формы
        await Login.fillLogin(invalid.email);
        await Login.fillPassword(invalid.password);
        await Login.submitForm();

        //Ожидание завершения с ошибкой запроса на авторизацию
        const response = await page.waitForResponse(`**\/users/login`);
        await expect(response.status()).toBe(401)

        //Проверка соотвествия фактического текста ошибки и ожидаемого
        const errorMessageText = await Login.getErrorMessage();
        await expect(errorMessageText).toContain(errorMessage);
      },
    );
  }
});
